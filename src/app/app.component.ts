import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'meet-ng-2';

  name = 'Mateusz'
  surname = 'Stradałowicz'
  address = 'Handlowa 6g'
}
